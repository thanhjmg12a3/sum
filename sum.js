function sumStrings(num1, num2) {
 
  let n1 = num1.split('').reverse().map(Number);
  let n2 = num2.split('').reverse().map(Number);
  while (n1.length < n2.length) {
    n1.push(0);
  }
  while (n2.length < n1.length) {
    n2.push(0);
  }
  let carry = 0 ;
  let result = [];
  for (let i = 0; i < n1.length; i++) {
    let sum = n1[i] +  n2[i] + carry;
    
    let digit = sum %10;
    
    carry = Math.floor(sum/10);
    result.unshift(digit);
  }
  if (carry > 0) {
    result.unshift(carry);
  }

  
  return result.join('');

 
  
}
let result = sumStrings("1234", "897");
console.log(result)